package main

import (
	"gitlab.com/karekarenn93/service_go/config"
	"gitlab.com/karekarenn93/service_go/router"
	"go.uber.org/zap"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(1)

	logger, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}

	cfg, err := config.LoadConfig(
		config.GetEnvOrDefault("CONFIG_PATH", "config.toml"), logger)
	if err != nil {
		logger.Panic(err.Error())
	}

	rout, err := router.NewRouter(cfg, logger)
	if err != nil {
		logger.Panic(err.Error())
	}
	go rout.Run()

	err = <-rout.GetErrorChan()
	wg.Wait()
}
